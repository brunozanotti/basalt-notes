import numpy as np
from scipy.linalg import cholesky

class LinearizationBase:
    class Options:
        def __init__(self):
            self.lb_options = LandmarkBlock.Options()
            self.linearization_type = None

    def __init__(self):
        self.linearization_type = None
        self.lb_options = None

    def linearizeProblem(self, out_numerically_valid):
        # TODO: Implement linearizeProblem
        pass

    def performQR(self):
        # TODO: Implement performQR
        pass

    def backSubstitute(self, pose_inc):
        # TODO: Implement backSubstitute
        pass

    def get_dense_Q2Jp_Q2r(self, Q2Jp, Q2r):
        # TODO: Implement get_dense_Q2Jp_Q2r
        pass

    def get_dense_H_b(self, H, b):
        # TODO: Implement get_dense_H_b
        pass

    @staticmethod
    def create(estimator, aom, options, marg_lin_data=None, imu_lin_data=None, used_frames=None, lost_landmarks=None,
               last_state_to_marg=float('inf')):
        # TODO: Implement create
        pass

class LinearizationAbsQR:
    def __init__(self):
        self.host_to_idx_ = {}  # Arbitrary order of host kfs (in where?)
        self.host_to_landmark_block = {}  #
        self.relative_pose_lin = {}
        self.relative_pose_per_host = {}
        self.landmark_ids = []
        self.landmark_block_idx = []
        self.num_rows_Q2r = 0
        self.imu_blocks = []
        self.pose_damping_diagonal = None
        self.pose_damping_diagonal_sqrt = None
        self.marg_scaling = None

class LandmarkBlockAbsDynamic:
    class State:
        Uninitialized = 0
        Linearized = 1
        NumericalFailure = 2
        Marginalized = 3

    def __init__(self):
        self.storage = None
        self.Jl_col_scale = np.ones(3)
        self.damping_rotations = []
        self.pose_lin_vec = []
        self.pose_tcid_vec = []
        self.padding_idx = 0
        self.padding_size = 0
        self.lm_idx = 0
        self.res_idx = 0
        self.num_cols = 0
        self.num_rows = 0
        self.options_ = None
        self.state = None
        self.lm_ptr = None
        self.calib_ = None
        self.aom_ = None
        self.res_idx_by_abs_pose_ = {}

class ImuBlock:
    def __init__(self):
        self.frame_ids = [0, 0]
        self.Jp = None
        self.r = None
        self.imu_meas = None

def optimize():
    if not opt_started or len(frame_states) <= 4:
        return

    opt_started = True
    aom = AbsOrderMap()
    for t, pose in frame_poses:  # Keyframe poses (s_k), empty before marginalize.
        aom.abs_order_map[t] = (aom.total_size, POSE_SIZE)
        aom.total_size += POSE_SIZE  # 6
        aom.items += 1
    for t, state in frame_states:  # Frame poses (s_f)
        aom.abs_order_map[t] = (aom.total_size, POSE_VEL_BIAS_SIZE)
        aom.total_size += POSE_VEL_BIAS_SIZE  # 15
        aom.items += 1

    # setup landmark blocks
    lqr_options = LinearizationBase.Options()
    lqr_options.lb_options.huber_parameter = config.huber_thresh  # 1.0
    lqr_options.lb_options.obs_std_dev = config.obs_std_dev  # 0.5
    lqr_options.linearization_type = config.vio_linearization_type  # ABS_QR
    ild = ImuLinData()
    ild.g = g
    ild.gyro_bias_sqrt_weight = gyro_bias_sqrt_weight
    ild.accel_bias_sqrt_weight = accel_bias_sqrt_weight
    ild.imu_meas = {}
    for t, m in imu_meas.items():
        ild.imu_meas[t] = imu_meas[t]
    lqr = LinearizationBase.create(estimator, aom, lqr_options, marg_data=marg_data, imu_lin_data=ild)

    lambda = config.vio_lm_lambda_initial  # 1e-4
    terminated = False
    converged = False
    message = ""

    it = 0
    it_rejected = 0
    while it <= config.vio_max_iterations(7) and not terminated:
        error_total, numerically_valid = lqr.linearizeProblem()

        # reset damping and scaling (might be set from previous iteration)
        pose_damping_diagonal = 0
        pose_damping_diagonal_sqrt = 0
        marg_scaling = np.zeros(aom.total_size)

        # Linearize relative poses
        for tcid_h, target_map in lmdb.getObservations():
            for tcid_t, _ in target_map:
                rpl = relative_pose_lin[(tcid_h, tcid_t)]

                if tcid_h != tcid_t:
                    state_h = estimator.getPoseStateWithLin(tcid_h.frame_id)
                    state_t = estimator.getPoseStateWithLin(tcid_t.frame_id)

                    T_t_h, d_rel_d_h, d_rel_d_t = computeRelPose(state_h.getPoseLin(), state_t.getPoseLin(), calib, rpl)

                    # if either already linearized, then current state guess is different from the state
                    # at which linearization happened. We need to recompute T_t_h from the current guess.
                    if state_h.isLinearized() or state_t.isLinearized():
                        T_t_h = computeRelPose(state_h.getPose(), state_t.getPose(), calib)

                    rpl.T_t_h = T_t_h

                else:
                    rpl.T_t_h = np.identity(4)
                    rpl.d_rel_d_h = np.zeros((6, 6))
                    rpl.d_rel_d_t = np.zeros((6, 6))

        error = 0
        valid = True

        # Linearize landmarks
        for r, (tcid_t, obs) in enumerate(lm_ptr.obs):
            error_sum = landmark_blocks[r].linearizeLandmark()

            storage.fill(0)
            numerically_valid = True

            for i, (tcid_t, obs) in enumerate(lm_ptr.obs):
                if pose_lin_vec[i] is None:
                    continue

                cam_t = calib.intrinsic[tcid_t.cam_id]
                h, t = pose_tcid_vec[i][0], pose_tcid_vec[i][1]
                h_idx, t_idx = aom[h].idx, aom[t].idx
                T_t_h, d_rel_d_t = pose_lin_vec[i].T_t_h, pose_lin_vec[i].d_rel_d_t

                res, d_res_d_xi, d_res_d_l = lm_ptr.cameraProjectionResidual(cam_t, T_t_h, obs)
                t_cam = cam_t.cam

                if not numerically_valid:
                    continue

                if config.use_robust_norm:
                    huber_thresh = config.huber_thresh
                    loss, d_loss_d_res = robustifyLoss(res, huber_thresh)
                    error_sum += loss
                else:
                    loss = res.transpose() @ res
                    error_sum += loss

                d_loss_d_res *= 2

                if config.use_log_residuals:
                    # Recover the original (non-log) residuals
                    res = np.exp(res) - 1.0

                d_loss_d_res *= obs.inverseDepthSigma2()

                # Robustification
                if config.use_robust_norm:
                    loss, d_loss_d_res = robustifyLoss(loss, huber_thresh)
                    error_sum += loss
                else:
                    loss = 0.5 * loss * loss
                    error_sum += loss

                # Store residual and Jacobians
                if pose_lin_vec[i].state == LandmarkBlockAbsDynamic.State.NumericalFailure:
                    numerically_valid = False

                if d_loss_d_res is not None:
                    d_res_d_xi = d_res_d_xi * t_cam

                    H.block_dAddition_i_j(d_loss_d_res, i, i)
                    H.block_dAddition_j_i(d_loss_d_res, i, i)

                    if config.vio_debug:
                        assert (np.all(np.isfinite(d_loss_d_res)))

                if d_res_d_xi is not None:
                    d_res_d_xi = d_res_d_xi * t_cam

                    # Add the Jacobians to the blocks
                    Jp[h_idx][i] += d_loss_d_res @ d_res_d_xi.transpose()  # dx
                    Jp[t_idx][i] -= d_loss_d_res @ d_res_d_xi.transpose()  # dx

                if d_res_d_l is not None:
                    H.block_dAddition_i_j(d_loss_d_res @ d_res_d_l, i, marg_size + r)
                    H.block_dAddition_j_i(d_loss_d_res @ d_res_d_l, i, marg_size + r)

                if config.vio_debug:
                    assert (np.all(np.isfinite(Jp[h_idx][i])))
                    assert (np.all(np.isfinite(Jp[t_idx][i])))

            if not numerically_valid:
                error += float('inf')
                valid = False

            if config.use_robust_norm:
                error += error_sum
            else:
                error += error_sum * error_sum

            # Save for debugging
            lm_idx = 0
            for i, (tcid_t, obs) in enumerate(lm_ptr.obs):
                lm_idx = i
                if pose_lin_vec[i].state != LandmarkBlockAbsDynamic.State.NumericalFailure:
                    break
            # Save landmarks in the visualization window
            if r < config.vio_debug_num_lms_per_host(50):
                d_res_d_xi = None
                if r == 0:
                    state_vec = []
                    for state in frame_states:
                        state_vec.append(state.get())
                    debugRes.__add_landmarks__(obs.get().y,
                                               obs.get().inverseDepth,
                                               obs.get().obs_std_dev,
                                               lm_ptr.inverse_depth_host[lm_idx],
                                               obs.get().lm_id,
                                               obs.get().lm_id,
                                               tcid_t.frame_id,
                                               state_vec,
                                               debugRes.CLOSED_LANDMARK,
                                               obs.get().lm_id + obs.get().lm_id,
                                               -1)
                else:
                    debugRes.__add_landmarks__(obs.get().y,
                                               obs.get().inverseDepth,
                                               obs.get().obs_std_dev,
                                               lm_ptr.inverse_depth_host[lm_idx],
                                               obs.get().lm_id,
                                               obs.get().lm_id,
                                               tcid_t.frame_id,
                                               [],
                                               debugRes.CLOSED_LANDMARK,
                                               obs.get().lm_id + obs.get().lm_id,
                                               -1)

        if valid and config.vio_debug:
            for i, (tcid_t, obs) in enumerate(lm_ptr.obs):
                if pose_lin_vec[i].state == LandmarkBlockAbsDynamic.State.NumericalFailure:
                    debugRes.__add_landmarks__(obs.get().y,
                                               obs.get().inverseDepth,
                                               obs.get().obs_std_dev,
                                               lm_ptr.inverse_depth_host[i],
                                               obs.get().lm_id,
                                               obs.get().lm_id,
                                               tcid_t.frame_id,
                                               [],
                                               debugRes.CLOSED_LANDMARK,
                                               obs.get().lm_id + obs.get().lm_id,
                                               -1)

        if not valid:
            error = float('inf')

        # add marginalization residuals
        if valid and marg_data and marg_data.marginalization_active:
            marg_scaling.fill(0)
            marg_data.applyMarginalization(marg_scaling)

            scaling_pose, scaling_vel = (1, 1)
            if config.vio_enforce_same_sign_velocity:
                scaling_pose, scaling_vel = enforceSameSignVelocity(pose0_vec, vel0_vec)

            error += marg_data.marginalizationResidual(scaling_pose, scaling_vel)

        # add imu residuals
        if valid and (imu_block is not None) and (imu_block.numImuResiduals() > 0):
            # Rescale prior
            error += (lambda * (imu_block.residual()).transpose() @ imu_block.residual()).trace()

            for b in imu_block.getImuResiduals():
                error += (lambda * (b.r).transpose() @ b.r).trace()

        if valid and config.vio_debug:
            if it_rejected == 0:
                for i, (tcid_t, obs) in enumerate(lm_ptr.obs):
                    debugRes.__add_landmarks__(obs.get().y,
                                               obs.get().inverseDepth,
                                               obs.get().obs_std_dev,
                                               lm_ptr.inverse_depth_host[i],
                                               obs.get().lm_id,
                                               obs.get().lm_id,
                                               tcid_t.frame_id,
                                               [],
                                               debugRes.CLOSED_LANDMARK,
                                               obs.get().lm_id + obs.get().lm_id,
                                               -1)
        error_total = error  # Store the total error to track its change

        # Error is small, convergence is reached
        if not valid:
            converged = True

        if not converged:
            lambda *= config.vio_lm_lambda_factor(5.0)

        if not converged and error_total > error_last:
            error_last = error_total

        error_last = error_total
        it += 1

    # Solve optimization problem with QR
    if config.vio_optimize(1):
        # Compute QR decomposition
        A = lqr.get_dense_Q2Jp_Q2r(lqr.Q2Jp, lqr.Q2r)
        b = lqr.get_dense_H_b(lqr.H, lqr.b)
        cholesky_decomposition = cholesky(A, lower=True)
        y = np.linalg.solve(cholesky_decomposition, b)

        # Perform back-substitution
        dx = np.linalg.solve(cholesky_decomposition.T, y)

        # Extract pose increments from dx
        pose_inc = []
        for i in range(0, num_frames):
            dp = d_pose.segment(i * 7, 7)
            delta_p = dx.segment(i * 6, 6)
            inc = SE3(delta_p) * dp
            pose_inc.append(inc)
    else:
        # Matrix form of the optimization
        A = np.zeros((6 * num_frames, 6 * num_frames))
        B = np.zeros((6 * num_frames, 6))
        C = np.zeros((6 * num_frames, 6 * num_frames))
        d = np.zeros((6 * num_frames, 1))

        for i in range(num_frames):
            for j in range(num_frames):
                A_ij = lqr.get_dense_Jp1_Jp2(i, j, lqr.Jp1, lqr.Jp2)
                A[(6 * i):(6 * (i + 1)), (6 * j):(6 * (j + 1))] = A_ij

            B_i = lqr.get_dense_Jp_i(i, lqr.Jp1)
            B[(6 * i):(6 * (i + 1)), :] = B_i

            C_i = lqr.get_dense_Jp_i(i, lqr.Jp2)
            C[(6 * i):(6 * (i + 1)), (6 * i):(6 * (i + 1))] = C_i

            d_i = lqr.get_dense_res_i(i, lqr.b)
            d[(6 * i):(6 * (i + 1)), :] = d_i

        # Linear system
        A += C + config.vio_lambda(lambda) * np.identity(6 * num_frames)
        dx = np.linalg.solve(A, -d)

        # Extract pose increments from dx
        pose_inc = [SE3(dx[i * 6:i * 6 + 6]) for i in range(num_frames)]

    # Update the poses
    for i in range(num_frames):
        frame_states[i].updatePose(frame_states[i].getState() * pose_inc[i])

    # Update the landmarks
    if valid:
        lm_ptr.incUpdateCounter()

        for i, (tcid_t, obs) in enumerate(lm_ptr.obs):
            if pose_lin_vec[i].state != LandmarkBlockAbsDynamic.State.NumericalFailure:
                landmark = lm_ptr.landmark_host[i]
                inverse_depth = lm_ptr.inverse_depth_host[i]

                # Update the landmark position
                d_res_d_xi = None
                if config.vio_debug:
                    d_res_d_xi = np.zeros((3, 6))
                else:
                    d_res_d_xi = np.zeros((obs.get().lm_dim, 6))

                for cam_id, cam_obs in enumerate(obs.get().observations):
                    if cam_obs.state == ObsBase.State.Valid:
                        cam_t = calib.get_cam_t_wcs(tcid_t.camera_index)
                        xi = frame_states[frame_id_t].getState().inverse()
                        yi = xi.inverse() @ tcid_t.frame_id.getState() @ cam_obs.y

                        if config.use_log_residuals:
                            # Log residuals
                            res = logResidual(
                                yi,
                                cam_obs.y,
                                cam_obs.inverseDepth,
                                cam_obs.inverseDepth_std_dev,
                            )
                        else:
                            # Standard residuals
                            res = yi - cam_obs.y

                        d_res_d_xi = cam_obs.d_res_d_y @ cam_obs.d_y_d_xi
                        res = -d_res_d_xi.transpose() @ res

                        if config.use_robust_norm:
                            huber_thresh = config.huber_thresh
                            res, _ = robustifyLoss(res, huber_thresh)

                        d_res_d_xi = -d_res_d_xi.transpose()

                        # Robustification
                        if config.use_robust_norm:
                            res, d_res_d_xi = robustifyLoss(res, huber_thresh)

                        d_res_d_xi = res @ d_res_d_xi

                        d_res_d_xi = d_res_d_xi * cam_obs.obs_std_dev
                        d_res_d_xi = d_res_d_xi * cam_obs.obs_std_dev

                        if config.vio_debug:
                            assert (np.all(np.isfinite(d_res_d_xi)))

                # Landmark Jacobians
                H.block_dAddition_i_j(d_res_d_xi, i, i)
                H.block_dAddition_j_i(d_res_d_xi, i, i)

                if config.vio_debug:
                    assert (np.all(np.isfinite(H.block_dAddition_j_i(d_res_d_xi, i, i))))
                    assert (np.all(np.isfinite(H.block_dAddition_i_j(d_res_d_xi, i, i))))

                # Robustification
                if config.use_robust_norm:
                    res, d_res_d_xi = robustifyLoss(res, huber_thresh)
                    error_sum += res
                else:
                    error_sum += res

    return error_total, error_sum
def computeIncrementalOptimization(  # noqa C901
        frame_id0: FrameId,
        frame_id1: FrameId,
        imu_meas: Optional[ImuQueue],
        bias_lin: Vector3d,
        bias_ang: Vector3d,
        marginalization_flag: int,
        delta_t: float,
        config: Config,
        frame_states: List[FrameBaseState],
        lm_ptr: Optional[LandmarkBlockAbsDynamic],
        calib: Optional[Calibration],
        imu_block: Optional[ImuBlock],
        pose_lin_vec: List[LandmarkBlockAbsDynamic.PoseLinData],
        marg_data: Optional[MargData],
        r: int,
        H: BlockMatrix,
        lm_hessian: BlockMatrix,
        vel_update: bool,
        debugRes: Optional[DebugRes],
        lambda_reset_param: Optional[Ref],
        prev_marg_data: Optional[MargData],
        imu_prev_block: Optional[ImuBlock],
) -> Tuple[List[FrameBaseState], MargData]:
    num_frames = len(frame_states)
    # Jacobians for preintegration
    if imu_meas and calib:
        imu_meas.addBias(
            frame_id0,
            bias_lin,
            bias_ang,
            frame_id0,
            bias_lin,
            bias_ang,
            bias_lin,
            bias_ang,
            bias_lin,
            bias_ang,
        )

        lin_p_i = bias_lin + calib.getGyroBiasLin() + calib.getAccelBiasLin()
        ang_p_i = bias_ang + calib.getGyroBiasAng() + calib.getAccelBiasAng()

        # Preintegrate measurements between the two frames
        imu_meas.preIntegrate(
            frame_id0,
            frame_id1,
            lin_p_i,
            ang_p_i,
            config.noise_gyro,
            config.noise_accel,
            config.noise_gyro_bias,
            config.noise_accel_bias,
            delta_t,
            frame_id1,
            calib,
            frame_states[frame_id0],
        )

        # Get preintegrated measurements
        preint_meas = imu_meas.getPreintMeas(frame_id1)
        imu_block.push_back(preint_meas)

        if imu_block.numImuResiduals() > 0:
            H.block_dAddition_i_j(imu_block.J, r + 1, r + 1)
            H.block_dAddition_j_i(imu_block.J.transpose(), r + 1, r + 1)
            H.block_dAddition_j_i(imu_block.J.transpose(), r, r)
            H.block_dAddition_i_j(imu_block.J, r, r)
            H.block_dAddition_i_j(imu_block.J.transpose(), r + 1, r)
            H.block_dAddition_j_i(imu_block.J, r + 1, r)

            if r > 0:
                H.block_dAddition_j_i(imu_block.J.transpose(), r - 1, r - 1)
                H.block_dAddition_i_j(imu_block.J, r - 1, r - 1)
                H.block_dAddition_j_i(imu_block.J, r - 1, r)
                H.block_dAddition_i_j(imu_block.J.transpose(), r - 1, r)

        # Add to the IMU Hessian
        lm_hessian.block_dAddition_i_j(imu_block.H, r + 1, r + 1)
        lm_hessian.block_dAddition_j_i(imu_block.H.transpose(), r + 1, r + 1)
        lm_hessian.block_dAddition_i_j(imu_block.H.transpose(), r, r + 1)
        lm_hessian.block_dAddition_j_i(imu_block.H, r, r + 1)
        lm_hessian.block_dAddition_i_j(imu_block.H.transpose(), r, r)
        lm_hessian.block_dAddition_j_i(imu_block.H, r, r)

    # Initialize linearization options
    lqr_options = LinearizationBase.Options()
    lqr_options.lb_options.huber_parameter = config.huber_thresh
    lqr_options.lb_options.obs_std_dev = config.obs_std_dev
    lqr_options.linearization_type = config.vio_linearization_type
    ild = ImuLinData()
    ild.g = gravity()
    ild.gyro_bias_sqrt_weight = config.gyro_bias_sqrt_weight
    ild.accel_bias_sqrt_weight = config.accel_bias_sqrt_weight
    ild.imu_meas = {}
    if imu_meas:
        for t, m in imu_meas:
            ild.imu_meas[t] = m
    lqr = LinearizationBase.create(
        config.vio_estimator, frame_states[frame_id0], lqr_options, marg_data, ild
    )

    # Create pose damping vector
    pose_damping_diagonal = np.zeros(7)
    pose_damping_diagonal[0:3] = config.vio_pos_damping
    pose_damping_diagonal[3:6] = config.vio_att_damping
    pose_damping_diagonal_sqrt = np.sqrt(pose_damping_diagonal)

    # Perform optimization
    error_total, error_sum = optimize(
        False,
        config,
        frame_states,
        pose_lin_vec,
        marg_data,
        lm_hessian,
        H,
        debugRes,
        r,
        pose_damping_diagonal,
        pose_damping_diagonal_sqrt,
        vel_update,
    )

    if config.vio_debug:
        debugRes.print_full = False
        debugRes.storeFullInfo(
            error_total,
            0,
            0,
            0,
            0,
            r,
            frame_id0,
            len(lm_ptr.obs),
            config.vio_add_state_priors(0),
        )

    # Check if marginalization is active
    marg_active = False
    if marg_data and marginalization_flag > 0:
        marg_active = True

    # Update marginalization
    if marg_active:
        if marg_data.num_marginalization_ == 0:
            lambda_init = config.vio_lm_lambda_initial
            lambda_reset = lambda_reset_param[0]
            marg_data.startMarginalization(lambda_init, lambda_reset)

        marg_options = MargOptimizationOptions()
        marg_options.ordering_optimize_size = 50
        marg_options.fixed_lm_size = 50
        marg_options.ordering_rand_hessian = 5
        marg_options.error_increase_factor = 1.0
        marg_options.error_decrease_factor = 1.0

        marg_state = marg_data.getCurrentState()
        ordering_size = marg_data.getOrderingSize()
        marginalization_size = marg_data.getMarginalizationSize()
        marginalization_error = marg_data.getCurrentError()

        # Update marginalization
        if marginalization_flag > 0:
            if marginalization_size > 0 and ordering_size > 0:
                marg_data.updateMarginalization(
                    marginalization_size, ordering_size, marginalization_error
                )
            elif ordering_size > 0:
                marg_data.updateMarginalization(ordering_size)
            elif marginalization_size > 0:
                marg_data.updateMarginalization(marginalization_size)
            else:
                marg_data.updateMarginalization()

            # Perform marginalization
            if marg_state != MargState.Marg:
                marg_data.performMarginalization(
                    marginalization_size, ordering_size, ordering_size, marg_options
                )

        # Print marginalization info
        if config.vio_debug:
            num_states = len(frame_states)
            num_marg_states = marginalization_size
            num_fixed_lms = marg_data.getMarginalizationFixedLmsSize()
            num_marg_lms = len(lm_ptr.landmark_host)
            debugRes.storeMargInfo(
                num_states,
                num_marg_states,
                num_fixed_lms,
                num_marg_lms,
                marg_state,
                marginalization_size,
                marginalization_error,
                ordering_size,
                len(lm_ptr.obs),
                config.vio_add_state_priors(0),
            )

        # Reset marginalization
        if marg_state == MargState.Done:
            marg_data.resetMarginalization()
            marg_data.initMarginalization()

    # Correct states with the new poses and velocities
    for i in range(num_frames):
        frame_states[i].correctState()

    return frame_states, marg_data

def buildGlobalLin(
        frame_states: List[FrameBaseState],
        lm_ptr: LandmarkBlockAbsDynamic,
        calib: Calibration,
        pose_lin_vec: List[LandmarkBlockAbsDynamic.PoseLinData],
        config: Config,
        H: BlockMatrix,
        r: int,
        lm_hessian: BlockMatrix,
        marg_data: Optional[MargData],
        vel_update: bool,
        imu_block: Optional[ImuBlock],
        debugRes: Optional[DebugRes],
        lambda_reset_param: Optional[Ref],
) -> Tuple[List[FrameBaseState], MargData]:
    num_frames = len(frame_states)

    # Create the pose damping vector
    pose_damping_diagonal = np.zeros(7)
    pose_damping_diagonal[0:3] = config.vio_pos_damping
    pose_damping_diagonal[3:6] = config.vio_att_damping
    pose_damping_diagonal_sqrt = np.sqrt(pose_damping_diagonal)

    # Perform the optimization
    error_total, error_sum = optimize(
        True,
        config,
        frame_states,
        pose_lin_vec,
        marg_data,
