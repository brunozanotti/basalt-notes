from typing import Dict, Tuple, List

# Types

Timestamp = int
TimeCamId = Tuple[Timestamp, int]
Vec3 = TODO
Mat4 = TODO
Mat6 = TODO
IntegratedImuMeasurement = TODO
PoseVelBiasStateLin = TODO
PoseStateLin = TODO
Keypoint = TODO


class AbsOrderMap:
    abs_order_map: Dict[int, Tuple[int, int]]
    total_size: int = 0
    items: int = 0

    def __init__(self):
        self.abs_order_map = {}


class LandmarkDatabase:
    def __init__(self):
        raise NotImplementedError

    def getObservations(self) -> Tuple[TimeCamId, Tuple[TimeCamId, Keypoint]]:
        raise NotImplementedError


class RelPoseLin:
    T_t_h: Mat4
    d_rel_d_h: Mat6  # Deriv of T_ct_ch w.r.t. T_w_ih, see issue basalt#37
    d_rel_d_t: Mat6  # Deriv of T_ct_ch w.r.t. T_w_it, see issue basalt#37


PoseLinMap = Dict[Tuple[TimeCamId, TimeCamId], RelPoseLin]
LandmarkBlock = TODO
host_to_idx: Dict[TimeCamId, int] = {}  # Arbitrary order of host kfs (in where?)
host_to_landmark_block: Dict[TimeCamId, List[LandmarkBlock]] = {}
relative_pose_per_host: List[List[PoseLinMap]] = []
relative_pose_lin: PoseLinMap = {}
# vector<KeypointId> landmark_ids;
# vector<size_t> landmark_block_idx;
# size_t num_rows_Q2r;

# vector<ImuBlock*> imu_blocks;


# Scalar pose_damping_diagonal;
# Scalar pose_damping_diagonal_sqrt;
# VecX marg_scaling;
class LinearizationAbsQR:
    def __init__(self):
        global host_to_idx, host_to_landmark_block, relative_pose_lin, relative_pose_per_host

        host_to_idx = {
            tcid_h: i for i, (tcid_h, _) in enumerate(lmdb.getObservations())
        }
        host_to_landmark_block = {tcid_h: [] for (tcid_h, _) in lmdb.getObservations()}
        relative_pose_lin = {
            (tcid_h, tcid_t): RelPoseLin()
            for tcid_h, target_map in lmdb.getObservations()
            for tcid_t, obs in target_map
        }
        relative_pose_per_host = [
            [relative_pose_lin[tcid_h, tcid_t] for tcid_t, _ in target_map]
            for tcid_h, target_map in lmdb.getObservations()
        ]

        if used_frames:
            landmark_ids = [k for k, v in lmdb.getLandmarks() if v.host_kf.ts in used_frames];
        elif lost_landmarks:
            landmark_ids = [k for k, v in lmdb.getLandmarks() if k in lost_landmarks];
        else: landmark_ids = [k for k, v in lmdb.getLandmarks()];
        size_t num_landmarks = landmark_ids.size();
        landmark_blocks.resize(num_landmarks);



# Constants

POSE_SIZE = 6
POSE_VEL_BIAS_SIZE = 15

# Options


class LinearizationOptions:
    huber_parameter = 1.0  # config.huber_thresh
    obs_std_dev = 0.5  # config.obs_std_dev
    linearization_type = "ABS_QR"  # config.vio_linearization_type;


class ImuLinData:
    g: Vec3
    gyro_bias_Sqrt_weight: Vec3
    accel_bias_sqrt_weight: Vec3


# State

lmdb = LandmarkDatabase()
opt_started = False
frame_states: Dict[Timestamp, PoseVelBiasStateLin] = {}
frame_poses: Dict[Timestamp, PoseStateLin] = {}
imu_meas: Dict[Timestamp, IntegratedImuMeasurement] = {}


def initialize():
    raise NotImplementedError  # TODO@mateosss


def optimize(self):
    global opt_started
    if not opt_started or len(frame_states) <= 4:
        return
    opt_started = True

    aom = AbsOrderMap()  # Order of states in linear system -> sort by timestamp
    for t, _ in frame_poses:  # Keyframe poses (s_k), empty before marginalize.
        aom.abs_order_map[t] = (aom.total_size, POSE_SIZE)
        aom.total_size += POSE_SIZE
        aom.items += 1

    for t, _ in frame_states:  # Frame poses (s_f)
        aom.abs_order_map[t] = (aom.total_size, POSE_VEL_BIAS_SIZE)
        aom.total_size += POSE_VEL_BIAS_SIZE
        aom.items += 1

    # setup landmark blocks
    lqr = LinearizationAbsQR()
